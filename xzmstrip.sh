#!/bin/bash
# xzmstrip is a script that, based on selected module, creates new one without unnecessary files, such as:
# - all translations (with option to keep them for own language)
# - man pages
# - development-related files (headers, static libraries, ...)
# - documentation
# - Python cache
# It also can strip unnecessary data from executables (programs, shared libraries).
# Use this script on your own risk.
# created by i3slkiller <https://gitlab.com/i3slkiller/xzmstrip>

set -e

VERSION=1

XZCOMPARGS="-comp xz -b 256K -Xbcj x86"
ZSTDCOMPARGS="-comp zstd -b 256K -Xcompression-level 22"

VERBOSE=0
STRIPLOCALE=0
STRIPMAN=0
STRIPDEVEL=0
STRIPDOCS=0
STRIPPYCACHE=0
STRIPEXECUTABLE=0
COMPARGS=$XZCOMPARGS

showHelp() {
  echo "Usage: $0 [parameters] module.xzm [output.xzm]"
  echo ""
  echo "Arguments:"
  echo "  -h, --help                  show help"
  echo "  -l, --strip-locale          delete locale files"
  echo "  -k, --keep-locale lang      keep selected locale, eg. pl for Polish language"
  echo "  -m, --strip-man             delete man pages"
  echo "  -b, --strip-devel           delete development-related files"
  echo "  -d, --strip-docs            delete documentation files"
  echo "  -p, --strip-pycache         delete Python cache files"
  echo "  -e, --strip-executable      strip executable files"
  echo "  -a, --strip-all             strip all above"
  echo "  -c, --compression xz|zstd   select compression algorithm for module"
  echo "  -V, --verbose               show what exactly was done with module"
  echo "  -v, --version               show script version"
}

showVersion() {
  echo "xzmstrip version $VERSION <https://gitlab.com/i3slkiller/xzmstrip>"
}

OPTS=hvVlk:mbdpeac:
LONGOPTS=help,version,verbose,strip-locale,keep-locale:,strip-man,strip-headers,strip-docs,strip-pycache,strip-executable,strip-all,compression:

ARGS=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name $0 -- $@)
[ $? != 0 ] && exit 1

eval set -- $ARGS

while true; do
  case $1 in
    -h|--help) showHelp; exit;;
    -v|--version) showVersion; exit;;
    -V|--verbose) VERBOSE=1; shift;;
    -l|--strip-locale) STRIPLOCALE=1; shift;;
    -k|--keep-locale) KEEPLOCALE=$2; shift 2;;
    -m|--strip-man) STRIPMAN=1; shift;;
    -b|--strip-devel) STRIPDEVEL=1; shift;;
    -d|--strip-docs) STRIPDOCS=1; shift;;
    -p|--strip-pycache) STRIPPYCACHE=1; shift;;
    -e|--strip-executable) STRIPEXECUTABLE=1; shift;;
    -a|--strip-all) STRIPLOCALE=1; STRIPMAN=1; STRIPDEVEL=1; STRIPDOCS=1; STRIPPYCACHE=1; STRIPEXECUTABLE=1; shift;;
    -c|--compression)
      case $2 in
        xz) COMPARGS=$XZCOMPARGS;;
        zstd) COMPARGS=$ZSTDCOMPARGS;;
        *) echo "No valid compression format selected"; exit 1 ;;
      esac
      shift 2;;
    --) shift; break;;
    *) echo "Unknown error"; exit 1 ;;
  esac
done

if [ $UID != 0 ]; then echo "This script require root privileges"; exit 1; fi

showVersion

if [ $VERBOSE == 1 ]; then
  RMVERBOSE=-v
  STRIPVERBOSE=-v
  FINDRMVERBOSE=(-printf "removed '%h/%f'\n")
fi

if [ ! -z $1 ]; then
  if [[ $1 == *.xzm ]]; then
    XZMFILE="$1"
  else
    echo "$0: '$1' is not a module"
    exit 1
  fi
else
  echo "$0: no file selected"
  exit 1
fi

if [ ! -z $2 ]; then
  if [[ $2 == *.xzm ]]; then
    XZMOUT="$2"
  else
    echo "$0: output module name must end with .xzm"
    exit 1
  fi
else
  XZMOUT="${XZMFILE/.xzm/_stripped.xzm}"
fi

if [ $STRIPLOCALE == 0 ] && [ $STRIPMAN == 0 ] && [ $STRIPDEVEL == 0 ] && [ $STRIPDOCS == 0 ] && [ $STRIPPYCACHE == 0 ] && [ $STRIPEXECUTABLE == 0 ]; then
  echo "$0: missing strip argument"
  exit 1
fi

while true; do
  TMP="/tmp/tmp.$RANDOM"
  [ ! -d $TMP ] && break
done
mkdir -p "$TMP/module"

unsquashfs -f -dest "$TMP/module" "$XZMFILE"

if [ $STRIPLOCALE == 1 ]; then
  if [ -z $KEEPLOCALE ]; then
    echo "Deleting locale files"
    rm $RMVERBOSE -rf \
      "$TMP/module/usr/share/locale" \
      "$TMP/module/usr/local/share/locale"
  else
    echo "Deleting locale files except $KEEPLOCALE"
    find "$TMP/module/usr/share/locale" "$TMP/module/usr/local/share/locale" -mindepth 1 -maxdepth 1 ! -name $KEEPLOCALE -exec rm $RMVERBOSE -rf {} \; 2> /dev/null || true
  fi
fi

if [ $STRIPMAN == 1 ]; then
  echo "Deleting man pages"
  rm $RMVERBOSE -rf \
    "$TMP/module/usr/man" \
    "$TMP/module/usr/share/man" \
    "$TMP/module/usr/local/share/man"
fi

if [ $STRIPDEVEL == 1 ]; then
  echo "Deleting development related files"
  rm $RMVERBOSE -rf \
    "$TMP/module/usr/include" \
    "$TMP/module/usr/lib/pkgconfig" \
    "$TMP/module/usr/lib64/pkgconfig" \
    "$TMP/module/usr/local/include" \
    "$TMP/module/usr/local/lib/pkgconfig" \
    "$TMP/module/usr/local/lib64/pkgconfig" \
    "$TMP/module/usr/src" \
    "$TMP/module/usr/local/src"
  find "$TMP/module" -type f \( -name "*.o" -o -name "*.a" -o -name "*.la" \) -delete "${FINDRMVERBOSE[@]}"
fi

if [ $STRIPDOCS == 1 ]; then
  echo "Deleting documentation files"
  rm $RMVERBOSE -rf \
    "$TMP/module/usr/doc" \
    "$TMP/module/usr/share/doc" \
    "$TMP/module/usr/share/gtk-doc" \
    "$TMP/module/usr/local/share/doc" \
    "$TMP/module/usr/local/share/gtk-doc"
fi

if [ $STRIPPYCACHE == 1 ]; then
  echo "Deleting Python cache files"
  find "$TMP/module" -depth -type d -name __pycache__ -exec rm $RMVERBOSE -rf {} \;
fi

if [ $STRIPEXECUTABLE == 1 ]; then
  echo "Stripping executables"
  find "$TMP/module" | xargs file | grep -e "executable" -e "shared object" | grep ELF | cut -f 1 -d : | xargs strip $STRIPVERBOSE || true
fi

mksquashfs "$TMP/module" "$XZMOUT" $COMPARGS -noappend
rm -rf "$TMP"
